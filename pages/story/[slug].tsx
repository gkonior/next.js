import { getCompanies, getCompany } from "../api";
import Layout from "../../components/Layout";
import { NextPage } from "next";
import { Brands } from "../../interfaces/brands";
import styled from "styled-components";

type Props = {
  brand: Brands;
  description: string;
};

const CoverWrapper = styled.div`
  height: 700px;
  background-size: cover;
  background-position: center;
  position: relative;
  border-radius: 5px;
  padding: 30px 30px 50px 30px;
`;

const CoverPhoto = styled.div`
  background: url(${({ cover }) => cover});
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  top: 0;
  border-radius: 5px;
  background-size: cover;
  z-index: 0;
`;

const HeadingWrapper = styled.div`
  position: absolute;
  width: 70%;
  bottom: 50px;
  left: 50px;
  z-index: 2;
`;

const Title = styled.h2`
  font-size: 32px;
  color: #ffffff;
  font-family: Open Sans, sans-serif;
`;

const Description = styled.p`
  font-size: 14px;
  color: #ffffff;
  font-family: Open Sans, sans-serif;
`;

const BottomOverlay = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  height: 400px;
  background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0), #000);
  z-index: 1;
`;

function createMarkup(data) {
  return { __html: data };
}

const Post: NextPage<Props> = ({ brand, description }) => {
  return (
    <Layout
      pageTitle={`${brand.name}. That's our Brand Story.`}
      pageDescription={description}
    >
      <CoverWrapper>
        <CoverPhoto cover={brand.cover_photo_url} />
        <HeadingWrapper>
          <Title>About Us</Title>
          <Description
            dangerouslySetInnerHTML={createMarkup(brand.short_description)}
          />
        </HeadingWrapper>
        <BottomOverlay />
      </CoverWrapper>
    </Layout>
  );
};

export const getStaticPaths = async () => {
  const res = await getCompanies();
  const brandstories = await res.json();

  const paths = brandstories.map((item) => ({
    params: { slug: item.id },
  }));

  return { paths, fallback: false };
};

export const getStaticProps = async ({ params }) => {
  const res = await getCompany(params.slug);
  const brand = await res.json();
  const configData = await import(`../../siteconfig.json`);

  return { props: { brand, description: configData.default.description } };
};

export default Post;
