import Layout from "../components/Layout";
import { NextPage } from "next";
import CompaniesList from "../components/CompaniesList";
import { getCompanies } from "./api";
import { GetStaticProps } from "next";

type Props = {
  title: string;
  description: string;
  companies: [];
};

const Home: NextPage<Props> = ({ title, description, companies }) => {
  return (
    <div>
      <Layout pageTitle={title} pageDescription={description}>
        <h1>Just Brands IT</h1>
        <main>
          <CompaniesList companies={companies} />
        </main>
      </Layout>
      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: Open Sans, sans-serif;
        }
        nav a {
          text-decoration: none;
        }
        * {
          box-sizing: border-box;
        }
      `}</style>
    </div>

  );
};

export const getStaticProps: GetStaticProps = async () => {
  const res = await getCompanies();
  const json = await res.json();
  const configData = await import(`../siteconfig.json`);

  return {
    props: {
      title: configData.default.title,
      description: configData.default.description,
      companies: json,
    },
  };
};

export default Home;
