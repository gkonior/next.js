import Layout from "../components/Layout";
import {GetStaticProps, NextPage} from "next";

type Props = {
  description: string;
};

const About: NextPage<Props> = ({ description }) => {
  return (
    <Layout pageTitle={`Just Brands IT - About`} pageDescription={description}>
      <h1 className="title">About Just Join IT</h1>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  const configData = await import(`../siteconfig.json`);

  return {
    props: {
      description: configData.default.description,
    },
  };
}

export default About;
