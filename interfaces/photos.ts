export type Photos = {
  image_thumb_url: string;
  image_url: string;
};
