export type People = {
  description: string;
  name: string;
  profile_photo_url: string;
  title: string;
};
