export type Technologies = {
  color?: string;
  logo_url: string;
  name: string;
};