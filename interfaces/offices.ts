export type Offices = {
  city: string;
  country_code: string;
  latitude: number;
  longitude: number;
  street: string;
};
