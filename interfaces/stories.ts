export type Stories = {
  body: string;
  photo_url: string;
  title: string;
};
