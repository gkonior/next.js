import { Offices } from "./offices";
import { People } from "./people";
import { Photos } from "./photos";
import { Stories } from "./stories";
import { Technologies } from "./technologies";

export interface Brands {
  cover_photo_url: string;
  developers_size?: number;
  facebook_url?: string;
  id: string;
  industry: string;
  instagram_url?: string;
  linked_in_url?: string;
  logo_url: string;
  name: string;
  offices: Offices[];
  people: People[];
  photos: Photos[];
  profile_color?: string;
  short_description: string;
  size: string;
  stories: Stories[];
  technologies: Technologies[];
  twitter_url: string;
  type: string;
  url: string;
}
