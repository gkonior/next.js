import Grid from "@material-ui/core/Grid";
import styled from "styled-components";
import Link from "next/link";

const BrandStoryTile = styled(Grid)`
  border-radius: 5px;
  overflow: hidden;
  background: #fff;
  display: block;
  color: #37474f;
  box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.05), 0 1px 5px 0 rgba(0, 0, 0, 0.04);
`;

const CoverPhoto = styled.div`
  background: url(${({ cover }) => cover});
  width: 100%;
  height: 100%;
  background-size: cover;
  height: 200px;
`;

const DetailsWrapper = styled.div`
  padding: 8px 15px;
  position: relative;
  display: flex;
  align-items: center;
`;

const BasicInfo = styled.div`
  min-width: 50px;
  margin-right: 5px;
`;

const CompanyName = styled.div`
  font-size: 18px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`;

const Type = styled.div`
  vertical-align: text-bottom;
  color: #99a1ab;
  font-size: 12px;
`;

const CompaniesList = ({ companies }) => {
  return (
    <Grid container spacing={2}>
      {companies.map((company, i) => (
        <Link as={`/story/${company.id}`} href="/story/[slug]" key={company.id}>
          <Grid item xs={12} sm={12} md={6} lg={4}>
            <BrandStoryTile>
              <CoverPhoto cover={company.cover_photo_url} />
              <DetailsWrapper>
                <BasicInfo>
                  <CompanyName>{company.name}</CompanyName>
                  <Type>{company.type}</Type>
                </BasicInfo>
              </DetailsWrapper>
            </BrandStoryTile>
          </Grid>
        </Link>
      ))}
    </Grid>
  );
};

export default CompaniesList;
