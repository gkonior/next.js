import Link from "next/link";
import { Container } from "@material-ui/core";
// @ts-ignore
import logo from "../static/logo.png";
import styled from "styled-components";

const Wrapper = styled.div`
  background: #ffffff;
  position: sticky;
  top: 0;
  z-index: 1;
`;

const TopBar = styled.header`
  height: 80px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Logo = styled.img`
  cursor: pointer;
`;

const StyledLink = styled.a`
  padding: 0 12px;
  color: #99a1ab;
  font-size: 14px;
  font-weight: 600;
  line-height: 24px;
  cursor: pointer;
  transition: 0.3s;
  &:hover {
    color: #f48fb1;
  }
`;

const Header = () => {
  return (
    <Wrapper>
      <Container>
        <TopBar>
          <Link href="/">
            <Logo src={logo} alt="Justbrands logo" />
          </Link>
          <nav>
            <Link href="/">
              <StyledLink>Brands</StyledLink>
            </Link>
            <Link href="/about">
              <StyledLink>About</StyledLink>
            </Link>
          </nav>
        </TopBar>
      </Container>
    </Wrapper>
  );
};

export default Header;
