import Head from "next/head";
import Header from "./Header";
import Container from "@material-ui/core/Container";
import styled from "styled-components";

const Body = styled.div`
  background: #f3f6f8;
`;

const StyledContainer = styled(Container)`
  padding: 40px 0;
`;

export default function Layout({
  children,
  pageTitle,
  pageDescription,
}: {
  children: object;
  pageTitle?: string;
  pageDescription?: string;
}) {
  return (
    <Body>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content={pageDescription} />
        <title>{pageTitle}</title>
      </Head>
      <section>
        <Header />
        <StyledContainer>{children}</StyledContainer>
      </section>
    </Body>
  );
}
